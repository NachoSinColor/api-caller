#### Api Caller v2.0.0

This is a generalized library using Retrofit 2 and RxJava. This library helps to remove boilerplate code which is used in every api calls. The response from each api call has a certain format so it is easier to use this library than Retrofit 2.


**Changelog:**

+ v2.0.0 is major version change because it replaces volley as underlying library with Retrofit 2 and RxJava. Retrofit 2 and RxJava makes it easier to make api calls and is easier to understand.

+ Now there's no more **ApiCaller.java** class so when upgrading to v2.0.0 follow procedure described below:

The response from each api call should be of following format:

     BaseResponse<T> {
       // This object is of generic type so you can change its type according to api requirements.  
       T obj;

       // Status is included in each response from api call.
       // Status contains success status, code and message.
       Status status;
     }

**Usage:**

Create a retrofit service using **RxRetrofitService** class.
For e.g. Creating UserService:

    public interface UserService {

        @GET("/login")
        Observable<BaseResponse<User>> basicLogin();
    }

    UserService userService = RxRetrofitService.createRetrofitService("base_url", UserService.class);

*Replace "base_url" with api's base url.*


Now you can make api calls and chain operators like in RxJava. Calling each api requires you to use compose() operator of RxJava to transform BaseResponse object to api's required object if status returns successful. If status returns unsuccessful then error message is sent to onError method of Subscriber of RxJava.

**Sample Code:**

    userService.basicLogin()
                .compose(new DataMapperTransformer<User>()) // This needs to be called in each api calls for transforming data.
                .subscribe(new BaseObserver<User>() {
                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        final String errorMessage = ((ApiException) t).getErrorMessage();
                        Log.d(TAG, "onError: " + errorMessage);
                    }

                    @Override
                    public void onNext(User user) {
                        super.onNext(user);
                        // This indicates status returned successful, so now you can start using User data.
                    }
                });


You can also provide your own OkHttpClient to RetrofitService using this method:
    
    RxRetrofitService.createRetrofitService("base_url", UserService.class, client);

Custom OkHttpClient can be used to intercept request or responses. Request Interceptor can be used to provide data to server in each api call by adding data to headers.

There's also helpful method to intercept respone headers which can be used if we want to read headers from response and act on it. It can be used as:

    OkHttpClient client = RxRetrofitService.getHeadersInterceptorClient(new HeadersListener() {
            @Override
            public void onHeadersIntercepted(Headers headers) {
                final int count = headers.size();
                for (int i = 0; i < count; i++) {
                    final String headerName = headers.name(i);
                    final String headerValue = headers.value(i);
                    Log.d(TAG, "onHeadersIntercepted: " + headerName);
                    Log.d(TAG, "onHeadersIntercepted: " + headerValue);
                }
            }
        });

    UserService userService = RxRetrofitService.createRetrofitService("base_url",
                UserService.class, client);

    // Now you can call api similar as above.