package com.smartmobe.apicaller;

import com.google.gson.annotations.SerializedName;

/**
 * This is base response of our every api calls.
 * Each response consists of actual data and status.
 */
@SuppressWarnings("WeakerAccess")
public class BaseResponse<R> {

    @SerializedName("body")
    private R body;

    @SerializedName("status")
    private Status status;

    public R getBody() {
        return body;
    }

    public Status getStatus() {
        return status;
    }
}