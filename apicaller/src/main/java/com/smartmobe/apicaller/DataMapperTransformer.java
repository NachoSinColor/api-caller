package com.smartmobe.apicaller;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * This should be called using compose() operator of Observable on each api call.
 *
 * @param <R> type of data that is expected from api call.
 */
@SuppressWarnings("unused")
public final class DataMapperTransformer<R> implements Observable.Transformer<BaseResponse<R>, R> {
    @Override
    public Observable<R> call(Observable<BaseResponse<R>> observable) {
        return observable.flatMap(new ResponseMapper<R>())
                .observeOn(AndroidSchedulers.mainThread());
    }
}