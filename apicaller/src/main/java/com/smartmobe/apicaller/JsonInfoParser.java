package com.smartmobe.apicaller;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashish Kayastha on Jun 15.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class JsonInfoParser {

    private static final String TAG = "JsonInfoParser";

    private final String jsonInfo;

    public JsonInfoParser(final String jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

    public String getErrorMessageByKey(final String key) {
        String error = "";
        if (!TextUtils.isEmpty(jsonInfo)) {
            try {
                final JSONObject jsonObject = new JSONObject(jsonInfo);
                if (!TextUtils.isEmpty(key)) {
                    final JSONArray jsonArray = jsonObject.getJSONArray(key);
                    error = jsonArray.getString(0);
                }
            } catch (JSONException e) {
                Log.e(TAG, "getErrorMessageByKey: ", e);
            }
        }
        return error;
    }
}