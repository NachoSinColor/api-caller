package com.smartmobe.apicaller;

import rx.Observable;
import rx.functions.Func1;

/**
 * This should be called using flatMap() operator of Observable on each api call.
 * It transforms BaseResponse with some data and status fields to data only; this holds true
 * if the status is success from server. If the status is unsuccessful or non empty jsonInfo,
 * then it throws ApiException and it should be properly handled in onError method of Subscriber.
 *
 * @param <R> type of data that is expected from api call.
 */
@SuppressWarnings("WeakerAccess")
public final class ResponseMapper<R> implements Func1<BaseResponse<R>, Observable<R>> {
    @Override
    public Observable<R> call(BaseResponse<R> baseResponse) {
        final Status status = baseResponse.getStatus();
        if (!status.isSuccess()) {
            final JsonInfoParser parser = new JsonInfoParser(status.getJsonInfo());
            return Observable.error(new ApiException(status.getMessage(), status.getStatusKey(), parser));
        }

        return Observable.just(baseResponse.getBody());
    }
}