package com.smartmobe.apicaller;

/**
 * Created by Ashish Kayastha on Jan 05.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class ApiException extends RuntimeException {

    private final String message;
    private final String statusKey;
    private final JsonInfoParser jsonInfoParser;

    public ApiException(String message, String statusKey, JsonInfoParser jsonInfoParser) {
        this.message = message;
        this.statusKey = statusKey;
        this.jsonInfoParser = jsonInfoParser;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getStatusKey() {
        return statusKey;
    }

    public JsonInfoParser getParser() {
        return jsonInfoParser;
    }
}