package com.smartmobe.apicaller;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Ashish Kayastha on Mar 10.
 */
@SuppressWarnings("unused")
public final class RxUtils {

    private RxUtils() {
        throw new AssertionError("No instances");
    }

    /**
     * Creates a new composite subscription if passed subscription is either null or unsubscribed.
     *
     * @param subscription composite subscription
     * @return a composite subscription
     */
    public static CompositeSubscription getNewCompositeSubscription(CompositeSubscription subscription) {
        if (subscription == null || subscription.isUnsubscribed()) {
            return new CompositeSubscription();
        }

        return subscription;
    }

    /**
     * Unsubscribes from the subscription if it is not null. This stops the receipt
     * of notifications on the {@link Subscriber} that was registered when this Subscription was received.
     * This should be called from onPause/onDestroy of Activity or Fragment depending upon requirements.
     *
     * @param subscription subscription to unsubscribe from
     */
    public static void unsubscribe(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public static void unsubscribe(CompositeSubscription subscription) {
        if (subscription != null) {
            subscription.clear();
        }
    }
}