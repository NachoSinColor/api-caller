package com.smartmobe.apicaller;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static android.provider.UserDictionary.Words.LOCALE;

/**
 * Created by Ashish Kayastha on Jan 05.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class BaseHeaderInterceptor implements Interceptor {

    private static final String ANDROID = "android";
    private static final String PLATFORM = "platform";
    private static final String DEVICE_ID = "device-id";
    private static final String AUTHORIZATION = "Authorization";
    private static final String TAG = "BaseHeaderInterceptor";

    private static final String ERROR_TOKEN_EXPIRED = "ERROR_TOKEN_EXPIRED";
    private static final String ERROR_TOKEN_INVALID = "ERROR_TOKEN_INVALID";
    private static final String ERROR_TOKEN_BLACKLIST = "ERROR_TOKEN_BLACKLIST";

    private final Gson gson;
    private final String androidId;
    private final OkHttpClient okHttpClient;

    public BaseHeaderInterceptor(OkHttpClient okHttpClient, Gson gson) {
        this.okHttpClient = okHttpClient;
        this.gson = gson;
        androidId = getAndroidId();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = buildNewRequest(original);

        final String authToken = getAuthToken();
        if (TextUtils.isEmpty(authToken)) {
            final Response response = chain.proceed(builder.build());
            saveAuthToken(response);
            return response;
        }

        builder.header(AUTHORIZATION, authToken);
        Response newResponse = chain.proceed(builder.build());
        saveAuthToken(newResponse);

        return getNewTokenIfTokenHasExpired(authToken, chain, newResponse, builder);
    }

    protected Request.Builder buildNewRequest(Request original) {
        return original.newBuilder()
                .addHeader(PLATFORM, ANDROID)
                .addHeader(DEVICE_ID, androidId)
                .addHeader(LOCALE, getLocale());
    }

    protected abstract String getAuthToken();

    protected abstract void setAuthToken(String authToken);

    protected abstract String getAndroidId();

    protected abstract String getLocale();

    protected Response getNewTokenIfTokenHasExpired(String previousToken, Chain chain, Response response,
                                                    Request.Builder builder) throws IOException {

        /*
          Response object is consumed when we call .string() method so we cannot return
          same response object anymore so we will create a new response object below
         */
        final String jsonString = response.body().string();

        if (isTokenExpired(jsonString)) {
            synchronized (okHttpClient) {
                final String currentToken = getAuthToken();
                if (previousToken.equals(currentToken)) {
                    final Response refreshTokenResponse = refreshToken();
                    if (refreshTokenResponse.isSuccessful()) {
                        if (isTokenInvalidOrBlackList(refreshTokenResponse.body().string())) {
                            logOut();
                            return null;
                        }
                    } else {
                        return buildNewResponse(response, jsonString);
                    }
                }

                final String newToken = getAuthToken();
                if (!TextUtils.isEmpty(newToken)) {
                    response.close();
                    builder.header(AUTHORIZATION, newToken);
                    return chain.proceed(builder.build());
                }
            }
        }

        return buildNewResponse(response, jsonString);
    }

    private boolean isTokenExpired(String jsonString) throws IOException {
        return checkIfResponseCodeIsInvalid(jsonString, HttpsURLConnection.HTTP_UNAUTHORIZED, ERROR_TOKEN_EXPIRED);
    }

    private boolean isTokenInvalidOrBlackList(String jsonString) throws IOException {
        return checkIfResponseCodeIsInvalid(jsonString, HttpsURLConnection.HTTP_UNAUTHORIZED,
                ERROR_TOKEN_BLACKLIST, ERROR_TOKEN_INVALID);
    }

    protected boolean checkIfResponseCodeIsInvalid(String jsonString, int invalidCode, String... invalidStatusType)
            throws IOException {
        boolean isInvalidStatusCode = false;
        if (!TextUtils.isEmpty(jsonString) && invalidStatusType != null) {
            final BaseResponse<?> baseResponse = gson.fromJson(jsonString, BaseResponse.class);
            if (baseResponse != null) {
                final Status status = baseResponse.getStatus();
                final boolean isInvalidResponseCode = status.getResponseCode() == invalidCode;

                for (String invalidType : invalidStatusType) {
                    isInvalidStatusCode = isInvalidResponseCode && invalidType.equals(status.getStatusKey());
                    if (isInvalidStatusCode) {
                        break;
                    }
                }
            }
        }

        return isInvalidStatusCode;
    }

    /**
     * We cannot read response body more than once so create a new response
     * with cached response string and response body content type
     */
    protected Response buildNewResponse(Response response, String jsonString) {
        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), jsonString))
                .build();
    }

    private Response refreshToken() {
        Response newResponse = null;
        try {
            Request request = buildRefreshTokenRequest();

            // Make synchronous call to get a new valid token
            newResponse = okHttpClient.newCall(request).execute();
            saveAuthToken(newResponse);
        } catch (IOException e) {
            Log.e(TAG, "Failed to refresh token", e);
        }

        return newResponse;
    }

    protected Request buildRefreshTokenRequest() {
        return new Request.Builder()
                .url(getRefreshTokenUrl())
                .header(PLATFORM, ANDROID)
                .header(DEVICE_ID, androidId)
                .header(LOCALE, getLocale())
                .header(AUTHORIZATION, getAuthToken())
                .build();
    }

    private void saveAuthToken(Response response) {
        if (response.isSuccessful()) {
            final String authToken = response.header(AUTHORIZATION);
            if (!TextUtils.isEmpty(authToken)) {
                setAuthToken(authToken);
            }
        }
    }

    protected abstract String getRefreshTokenUrl();

    protected abstract void logOut();
}