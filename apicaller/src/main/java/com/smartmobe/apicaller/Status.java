package com.smartmobe.apicaller;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashish Kayastha on Mar 03.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Status {

    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private String message;

    @SerializedName("responseCode")
    private int responseCode;

    @SerializedName("responseTimeStamp")
    private String responseTimeStamp;

    @SerializedName("statusKey")
    private String statusKey;

    @SerializedName("jsonInfo")
    private String jsonInfo;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseTimeStamp() {
        return responseTimeStamp;
    }

    public String getStatusKey() {
        return statusKey;
    }

    public String getJsonInfo() {
        return jsonInfo;
    }
}