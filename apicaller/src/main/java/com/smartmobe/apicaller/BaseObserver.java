package com.smartmobe.apicaller;

import rx.Observer;

/**
 * Created by Ashish Kayastha on Mar 10.
 */
@SuppressWarnings("unused")
public class BaseObserver<T> implements Observer<T> {
    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable t) {
    }

    @Override
    public void onNext(T obj) {
    }
}