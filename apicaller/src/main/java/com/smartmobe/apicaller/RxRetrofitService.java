package com.smartmobe.apicaller;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by Ashish Kayastha on Mar 02.
 */
@SuppressWarnings("unused")
public final class RxRetrofitService {

    private static final OkHttpClient.Builder HTTP_CLIENT = new OkHttpClient.Builder();

    private static final Retrofit.Builder RETROFIT_BUILDER = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()));

    private RxRetrofitService() {
        throw new AssertionError("No instances");
    }

    /**
     * Build a retrofit service which works with RxJava and uses Gson to convert models.
     *
     * @param baseUrl base url for each api calls. Valid base url should end with "/".
     * @param client  custom OkHttpClient which can be used to intercept request/response.
     *                It can be null; if null then it builds client from above field and
     *                passes it to retrofit.
     * @return a retrofit service
     */
    @SuppressWarnings("WeakerAccess")
    public static Retrofit getRetrofit(@NonNull @Size(min = 1) String baseUrl, @Nullable OkHttpClient client) {
        if (client == null) {
            client = HTTP_CLIENT.build();
        }
        return RETROFIT_BUILDER.baseUrl(baseUrl).client(client).build();
    }

    /**
     * Build a retrofit service which works with RxJava and uses Gson to convert models.
     *
     * @param baseUrl base url for each api calls. Valid base url should end with "/".
     * @return a retrofit service
     */
    public static Retrofit getRetrofit(@NonNull @Size(min = 1) final String baseUrl) {
        return getRetrofit(baseUrl, null);
    }
}